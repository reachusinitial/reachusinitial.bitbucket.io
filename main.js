document.getElementById("name").onmouseover = function() { mouseOver() };
document.getElementById("name").onmouseout = function() { mouseOut() };

function mouseOver() {
    document.getElementById("name").style.color = "blue";
}

function mouseOut() {
    document.getElementById("name").style.color = "rgb(0, 38, 77)";
}

document.getElementById("message").onmouseover = function() { mouseOver2() };
document.getElementById("message").onmouseout = function() { mouseOut2() };

function mouseOver2() {
    document.getElementById("message").style.color = "blue";
}

function mouseOut2() {
    document.getElementById("message").style.color = "rgb(0, 38, 77)";
}